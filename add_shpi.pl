#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: add_shpi.pl
#
#        USAGE: ./add_shpi.pl  
#
#  DESCRIPTION: Этот скрипт добавляет все ШПИ по заданному диапазону в файл
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 13.08.2014 14:22:37
#     REVISION: ---
#===============================================================================
use utf8;
use strict;
use warnings;
use FindBin qw($Bin);
use lib $Bin;
use lib '/home/vrag/perl/Modules';
use DB;
use Rostelecom;
use open qw(:std :utf8);

our $DB_TABLE 	= 'shpi';


sub generate_shpi
{#Функция высчитывает контрольный 14-й разряд ШПИ. На вход получает 13 значный код
	my $code = shift;
	die "SHPI $code has wrong format" unless $code=~/^\d{13}$/; 
	my ($contr_digit, $sum);
	if ($code=~/(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)/)
	{
		$sum=($1+$3+$5+$7+$9+$11+$13)*3+($2+$4+$6+$8+$10+$12);

	}
	return $code.'0' if ($sum % 10)==0;
	while (($sum++) % 10)
	{
		$contr_digit++;
	}
	return "$code$contr_digit";
}



my $i = 0;
my $dbh = DB::connect(-db_name => 'rostelecom');


print "\nВведите диапазон ШПИ через -\n";
my $range = <STDIN>;
$range =~s /\n//g;
$range =~s / //g;
die "Неверный диапазон" unless $range =~ /^\d{13}-\d{13}$/;

my ($first_shpi, $last_shpi) = split /-/, $range;

print "Выберите регион ШПИ:\n1 - Москва\n2 - Питер\n3 - Новосибирск\n";
my $region_code = <STDIN>;
$region_code =~s /\n//g;
$region_code =~s / //g;
die "Неправильный выбор региона" unless ($region_code =~ /(1|2|3)/) ;

my $tek_shpi = $first_shpi;
my ($tek_num) = $first_shpi =~ /\d{8}(\d{5})/;
my ($last_num) = $last_shpi =~ /\d{8}(\d{5})/;
die "Неверный диапазон конец диапазона больше начала" if $last_num<$tek_num;


--$tek_num;
--$region_code;
my @str;
do
{
	$i++;
	$tek_num++;
	$tek_num = sprintf("%05d", $tek_num);
	$tek_shpi =~ s/^(\d{8}).*/$1$tek_num/;
	my $shpi = &generate_shpi($tek_shpi);
	push @str, "($shpi, $region_code)";
}
while ($tek_num != $last_num);

my $query = "INSERT IGNORE INTO $DB_TABLE (shpi, region_id) VALUES ". join (',', @str);
$dbh->do ($query) or die "Cant do query $query";

print "Generating $i shpi\n";


