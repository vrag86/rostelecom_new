#
#===============================================================================
#
#         FILE: Rostelecom.pm
#
#  DESCRIPTION: 
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 08.05.2015 11:35:58
#     REVISION: ---
#===============================================================================
package Rostelecom;
use utf8;
use strict;
use warnings;
use lib '/home/vrag/perl/Modules';
use DBI;
use XML::Simple;
use Data::Printer;
use AddressInfo_new;
use AnyEvent::Fork::Pool;
use Encode;
use AnyEvent::Fork;
use File::Path::Tiny;
use PDF_Features;
use PDF::API2;
use File::Basename;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Jabber;
use Dat;
use DB;
use F1032;
use App qw(create_app);
use ScpUpload; 
use Pdf2Afp;
use SendMail;
require Exporter;


our @EXPORT 		;
our @ISA      		= qw(Exporter);
our $VERSION 		= 2.00;

use constant mm => 25.4/72;

our $TEMP_DIR 				= '/var/tmp/rostelecom';
our $FONT_FILE           	= '/opt/perl/font/Times_New_Roman.ttf';
our $FONT_B_FILE         	= '/opt/perl/font/Times_New_Roman_bold.ttf';
our $ARIAL_FONT_FILE     	= '/opt/perl/font/ARICYR.ttf';
our $ARIAL_FONT_B_FILE   	= '/opt/perl/font/ARICYRB.ttf';
our $FORMDEF				= 'F1N20010';
our $DB_NAME	 			= 'rostelecom';

sub set_param
{#Переопределение значений глобальных переменных
	my %opt 			= @_;
	$DB_NAME	 		= $opt{-db_name} if exists $opt{-db_name};
	$TEMP_DIR 			= $opt{-temp_dir} if exists $opt{-temp_dir};
	$FORMDEF 			= $opt{-formdef} if exists $opt{-formdef};
	$FONT_FILE 			= $opt{-font_file} if exists $opt{-font_file};
	$FONT_B_FILE		= $opt{-font_b_file} if exists $opt{-font_b_file};
	$ARIAL_FONT_FILE	= $opt{-arial_font_file} if exists $opt{-arial_font_file};
	$ARIAL_FONT_B_FILE	= $opt{-arial_font_b_file} if exists $opt{-arial_font_b_file};
	return 1;
}

sub new 
{
	my $class 					= shift;
	my $self 					= {};
	my %opt						= @_;
	$self->{output_dir} 		= $opt{-output_dir} || do {warn "You must specify -output_dir for new class"; return 0};
	File::Path::Tiny::mk ($self->{output_dir}) or do {warn "Cant create $self->{output_dir}"; return 0};
	File::Path::Tiny::mk ($TEMP_DIR.'/pdf') or do {warn "Cant create $TEMP_DIR/pdf"; return 0};
	File::Path::Tiny::mk ($TEMP_DIR.'/f103') or do {warn "Cant create $TEMP_DIR/f103"; return 0};
	
	if (!exists $opt{-template} || !-e $opt{-template} || $opt{-template} !~ /\.pdf$/)
	{
		warn "You must specify good -template file";
		return 0;
	}
	my ($sec,$min,$hour,$mday,$mon,$year,undef,undef,undef) = localtime(time);
	$self->{dat}->{sec} 		= $sec;
	$self->{dat}->{min} 		= $min;
	$self->{dat}->{hour} 		= $hour;
	$self->{dat}->{mday} 		= $mday;
	$self->{dat}->{mon} 		= $mon+1;
	$self->{dat}->{year} 		= $year+1900;

	$self->{template} 			= $opt{-template};
	$self->{cpus} 				= $opt{-cpus} 				|| 4;
	$self->{company} 			= $opt{-company}			|| 'ОАО "Ростелеком"';
	$self->{inn}				= $opt{-inn} 				|| '7707049388';
	$self->{kpp}				= $opt{-kpp} 				|| '';
	$self->{post_index_sndr}	= $opt{-post_index_sndr}	|| '';
	$self->{sender_region}		= $opt{-sender_region}		|| '';
	$self->{sender_city}		= $opt{-sender_city}		|| '';
	$self->{short_company_name} = $opt{-short_company_name} || 'RTK';
	$self->{formatted_date}		= formatted_date() 			|| 'NOT_A_DATE',
	$self->{write_log}			= $opt{-write_log};
	$self->{write_error}		= $opt{-write_error};
	$self->{dbh} 				= DB::connect(-db_name => $DB_NAME); 
	bless $self, $class;
	return $self;
}


sub read_data
{#Считывает данные из переданного файла
	my $self 	= shift;
	my $fname 	= shift;
#		my $client 	= $self->{client};
	my $xml = eval {XMLin ($fname, SuppressEmpty => '')};
	if ($@)
	{
		$self->error($@); 
		return 0;
	}

	my ($reads_ok, $reads_bad) = (0, 0);
	foreach my $xml_dat (@{$xml->{mail}})
	{
		$xml_dat->{source_fname} = basename ($fname);
		$self->correct_data ($xml_dat) or return 0;
		my $apartment = '';
		if ($xml_dat->{apartment} =~ /./ && $xml_dat->{apartment} !~ /^-$/ && $xml_dat->{apartment} !~ /^0$/)
		{
			$apartment = "кв. $xml_dat->{apartment}";
		}
		my $post = check_address(join (", ", $xml_dat->{zip}, $xml_dat->{region}, $xml_dat->{district}, $xml_dat->{city}, $xml_dat->{street}, $xml_dat->{bulding1}, $xml_dat->{bulding2}, $xml_dat->{bulding3}, $apartment));
		if ($post)
		{
			my $region = $self->define_region($post->{post_index}) or return 0;
			$post->{name} 			= join (' ', $xml_dat->{lastname}, $xml_dat->{firstname}, $xml_dat->{middlename});
			$post->{lastname}       = $xml_dat->{lastname},
			$post->{firstname}      = $xml_dat->{firstname},
			$post->{middlename}     = $xml_dat->{middlename},
			$post->{code} 			= $xml_dat->{code};
			$post->{userid} 		= $xml_dat->{userid};
			$post->{source_fname} 	= $xml_dat->{source_fname};
			push @{$self->{client}->{$region}}, $post;

			++$reads_ok;
		}
		else
		{#Bad data
			push @{$self->{BAD}}, $xml_dat;
			++$reads_bad;
		}

	}

	return ($reads_ok, $reads_bad);
}

sub correct_data
{#Функция проверяет наличие всех полей в исходном xml и длину некоторых полей
	my $self 	= shift;
	my $xml_dat = shift;
	my @keys = qw/userid lastname firstname middlename code zip region district city street bulding1 bulding2 bulding3 apartment/;
	foreach my $key (@keys)
	{
		if (!exists $xml_dat->{$key})
		{
			$self->error("correct_data error $xml_dat->{source_fname} not have $key"); return 0;
		}
	}
	#Check code length
	if ($xml_dat->{code} !~ /^\d{16}$/)
	{
		$self->error("correct_data error $xml_dat->{source_fname} code $xml_dat->{code} have not 16 digit"); return 0;
	}
	return 1;
}

sub define_shpi
{#Определяет ШПИ код каждому клиенту
	my $self   = shift;
	my $client = $self->{client} || do {$self->error ('$self not have client'); return 0}; 
	my $dbh = $self->{dbh} or do {$self->error ('$self has not contain dbh'); return 0};
	my @all_shpi;    #Сюда запишем все полученные из базы ШПИ, чтобы потом их удалить
	#Вычисляем номер текущего месяца с 2000 года
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $month = (($year - 100)*12) % 99 + $mon + 1;

	#Проверим наличие необходимого количества ШПИ в базе
	foreach my $region (keys %$client)
	{
		my $shpi_count = get_shpi_count($dbh, $region);
		my $need_shpi = scalar (@{$client->{$region}});
		$self->write_log ("Database have $shpi_count shpi for region $region. Need shpi: $need_shpi");
		if ( $shpi_count < $need_shpi)
		{
			$self->error ("Database not have need shpi count. Need: $need_shpi. In DB: $shpi_count. Region: $region");
			return 0;
		}
		else
		{#Присвоим ШПИ каждому клиенту в данном регионе
			my $query = "select s.shpi from shpi s inner join region r on r.id=s.region_id where mod(ceiling(s.shpi/1000000)-1, 100) = $month and r.region='$region' LIMIT $need_shpi";
			my $sth = $dbh->prepare($query); $sth->execute;
			my $shpi_ref = $sth->fetchall_arrayref([0]);
			foreach my $letter (@{$client->{$region}})
			{
				my $shpi = shift @$shpi_ref or do {$self->error("Cant shift shpi_array. Not enought element"); return 0};
				$letter->{shpi} = $shpi->[0];
				push @all_shpi, $shpi;
			}

		}
	}

	#Delete getting shpi
	foreach (@all_shpi)
	{
		my $query = "DELETE FROM shpi where shpi=$_->[0]";
		$dbh->do($query) or do {$self->error("Cant delete shpi $_ from DB"); return 0};
	}

	return 1;

}


sub create_pdf
{
	my $self = shift;
	my %opt = @_;
	my $spool_size 	=  $opt{-spool_size}	|| do {$self->error("You must specify -spool_size for function create_pdf"); return 0};
	my $ops 		=  $opt{-ops}			|| do {$self->error("You must specify -ops hashref for function create_pdf"); return 0};
	my ($sec, $min, $hour, $mday, $mon, $year) = ($self->{dat}->{sec}, $self->{dat}->{min}, $self->{dat}->{hour}, $self->{dat}->{mday}, $self->{dat}->{mon}, $self->{dat}->{year});
	
	foreach my $region (keys %{$self->{client}})
	{
		my $array_ref = $self->{client}->{$region};
		my $output_dir = sprintf ("%s/%s/%s", $self->{output_dir}, $region, $self->{formatted_date});
		File::Path::Tiny::mk ($output_dir) or do {$self->error("Cant create $output_dir"); return 0};
		

		my $i = 0;
		my $num_in_spool = 0;
		my $pdf_temp_fname = [];
		my $f103_data 		= [];
		my ($pool, $finish) = create_pool('create1_pdf', $self->{cpus});
		my $out_fname = sprintf ("%s/%01d%02d%02d%02d.pdf", $output_dir, substr($year, length($year)-1, 1), $mon, $mday, ++$num_in_spool);
		foreach my $letter (@$array_ref)
		{
			my $temp_fname = sprintf ("%s/pdf/%06d.pdf", $TEMP_DIR, ++$i);
			$$pool->(
						-fname 			=> $temp_fname,
						-shpi 			=> $letter->{shpi},
						-template   	=> encode ('utf8', $self->{template}),
						-name 			=> encode ('utf8', $letter->{name}),
						-address		=> encode ('utf8', $letter->{address}),
						-post_index 	=> $letter->{post_index},
						-code			=> $letter->{code},
						-service_str	=> sprintf ("%s - %03d", basename ($out_fname, '.pdf'), $i),
						sub {print "Create $_[0]\n";
							}
					);
			#Вставим в массив имена временных файлов, из которых соберем потом один
			push @$pdf_temp_fname, $temp_fname;
			#Вставим в массив данные для создания формы 103
			push @$f103_data, {%$letter}; 	

			#Добавим служебную информацию каждому клиенту
			$letter->{out_fname} 	= $out_fname;
			
			#Create spool		
			if ( ($i % $spool_size) == 0 || ($i == scalar (@$array_ref)) )
			{
				undef $$pool; $$finish -> recv;
				my $f103 = F1032-> new (-list_num 	=> $num_in_spool, 
										-sender 	=> $self->{company},
										-inn 		=> $self->{inn},
										-kpp		=> $self->{kpp},
										-ops		=> $ops->{$region},
									) or do {$self->error ("Cant create F1032 obj"); return 0};
				$f103->generate_f103 (-data => $f103_data, -dir => sprintf ("%s/f103", $TEMP_DIR)) or do {$self->error ("Cant generate f103"); return 0};
				$f103->generate_ini18 (
										-dir 				=> $output_dir, 
										-data 				=> $f103_data,
										-post_index_sndr 	=> $self->{post_index_sndr},
										-sender_region 		=> $self->{sender_region},
										-sender_city 		=> $self->{sender_city},
									) or do {$self->error ("Cant generate f103 digital");return 0};
#				push @{$self->{ini}->{$region}}, $f103->{ini_fname};
#				$f103->generate_ini18 (-inn => $self->{inn}, -dir => $output_dir, -data => $f103_data) or do {$self->error ("Cant generate f103_v18 digital");return 0};
				push @$pdf_temp_fname, $f103->{pdf_fname};
				push @{$self->{ini}->{$region}}, $f103->{ini_fname};


				collect (-fname => $out_fname, -array => $pdf_temp_fname, -unlink => 1) or do {$self->error ("Cant collect files to $out_fname"); return 0};
				$self->write_log ("Created $out_fname");
				#Info for app
				push @{$self->{pdf}->{$region}}, {
													filename 		=> $out_fname,
													sheets_count 	=> $f103->{pdf_sheets} + scalar (@$pdf_temp_fname) - 1,
													sends_counts		=> scalar (@$pdf_temp_fname) - 1,
												};

				#Обновление переменных
				($pool, $finish) = create_pool('create1_pdf', $self->{cpus});
				$pdf_temp_fname = [];
				$f103_data 		= [];
				$out_fname = sprintf ("%s/%01d%02d%02d%02d.pdf", $output_dir, substr($year, length($year)-1, 1), $mon, $mday, ++$num_in_spool);
			}
		}

	}
	return 1;
}



sub create1_pdf
{
	my %opt 		= @_;
	my $fname 		= $opt{-fname};
	my $shpi 		= $opt{-shpi};
	my $name		= decode ('utf8', $opt{-name});
	my $code 		= $opt{-code};
	my $address 	= decode ('utf8', $opt{-address});
	my $post_index	= $opt{-post_index};
	my $service_str = $opt{-service_str};
	my $template 	= decode ('utf8', $opt{-template});

	my $pdf = PDF::API2 -> new (-file => $fname) or die "Cant create file $fname";
	my $pdf_template = PDF::API2 -> open ($template) or die "Cant open $template";
	my $page = $pdf -> importpage ($pdf_template, 1);
	$pdf_template -> end();
	
#Define main fonts
	my $times        = $pdf -> ttfont ($FONT_FILE);		
	my $times_b      = $pdf -> ttfont ($FONT_B_FILE);		
	my $arial_font   = $pdf -> ttfont ($ARIAL_FONT_FILE);		
	my $arial_font_b = $pdf -> ttfont ($ARIAL_FONT_B_FILE);		
#Add barcode
	draw_barcode (
					-pdf		=> $pdf,
					-page		=> $page,
					-shpi		=> $shpi,
					'-x'		=> 125/mm,
					) or die "Cant draw barcode";
	draw_address_block (
					-pdf		=> $pdf,
					-page		=> $page,
					-post_index	=> $post_index,
					-address	=> $address,
					-fio		=> $name,
					) or die "Cant draw address block";

#Добавим служебную строку
	my ($x, $y) = (125/mm, 261/mm);
	my $text = $page->text();
	$text->font($times, 8);
	$text->translate($x, $y)->text($service_str);	
	$text->translate($x - 106/mm, $y - 257.5/mm)->text($service_str);	
#Add firstname lastname middlename(Уважаемый client)
	($x, $y) = (95/mm, 202.5/mm);
	$text->font($times_b, 12)->translate($x, $y)->text("$name!");
	
#Add activation code
	($x, $y) = (35/mm, 168/mm);
	$text->font($times_b, 10)->translate($x, $y)->text($code);


	$pdf->save();
	$pdf->end();
	return basename($fname);
}

sub generate_rtk_xml
{#Генерирует XML ini файл для Ростелекома
	my $self 			= shift;
	my $xml_filenum 	= shift;
	my $dat 			= $self->{dat};
	my $xml_dir 		= sprintf ("%s/XML", $self->{output_dir});
	File::Path::Tiny::mk ($xml_dir) or do {$self->error ("Cant create $xml_dir"); return 0};

	my ($last_xml_date, $num) = split /;/, $xml_filenum;
	if ($self->{formatted_date} == $last_xml_date)
	{
		++$num;
	}
	else
	{
		$num = 1;
	}

	my $fname = sprintf ("%s/%04d%02d%02d%02d.xml", $xml_dir, $dat->{year}, $dat->{mon}, $dat->{mday}, $num);
	my $post_date = sprintf ("%02d.%02d.%04d %02d:%02d:%02d", $dat->{mday}, $dat->{mon}, $dat->{year}, $dat->{hour}, $dat->{min}, $dat->{sec});
	my ($all_bad, $all_good) = (0, 0);
	my $xml;
   	$xml->{Good} = {}; $xml->{Bad} = {};
	
	#Add GOOD section
	foreach my $region (keys %{$self->{client}})
	{
		foreach my $letter (@{$self->{client}->{$region}})
		{
			push @{$xml->{Good}->{Code}}, {
											USERID 		=> [$letter->{userid}],
											SHPI		=> [$letter->{shpi}],
											PostDate	=> [$post_date],
											PostName	=> [$letter->{source_fname}],
											Surname		=> [$letter->{lastname}],
											Name		=> [$letter->{firstname}],
											MiddleName	=> [$letter->{middlename}],
										};
			++$all_good;
		}
	}

	#Add Bad section
	foreach my $letter (@{$self->{BAD}})
	{
		push @{$xml->{Bad}->{Code}}, {
										USERID 		=> [$letter->{userid}],
										PostDate	=> [$post_date],
										PostName	=> [$letter->{source_fname}],
										Surname		=> [$letter->{lastname}],
										Name		=> [$letter->{firstname}],
										MiddleName	=> [$letter->{middlename}],
										Error		=> ['Неверно указан адрес'],
									};
		++$all_bad;
	}
	$xml->{TimeShtamp} 	= sprintf("%d/%d/%04d", $dat->{mday}, $dat->{mon}, $dat->{year});
	$xml->{FileNum} 	= sprintf("%02d", $num);
	$xml->{All} 		= $all_good + $all_bad;
   	$xml->{AllGood} 	= $all_good;
	$xml->{AllBad} 		= $all_bad;


	my $xs = XML::Simple->new();
	open my $FL, ">:encoding(utf8)", $fname or do {$self->error ("Cant open $fname $!"); return 0};
	print $FL $xs->XMLout ($xml, XMLDecl => '<?xml version="1.0" encoding="utf-8" standalone="yes"?>', RootName=>'CodeSHPI', SuppressEmpty => undef );
	close $FL;

	my $zip_fname = $fname;
   	$zip_fname =~ s/\.xml$/.zip/;
	my $zip = Archive::Zip->new();
	$zip->addFile ($fname, basename($fname), 9);
	if ($zip->writeToFileNamed ($zip_fname) == AZ_OK)
	{
		unlink $fname; 
		$fname = $zip_fname;
	}
	else
	{
		$self->write_log("Cant add xml $fname to zip archive $zip_fname");
	}

	$self->{xml_fname} = $fname;
		

	$self->{xml_filenum} = join (';', $self->{formatted_date}, sprintf ("%02d", $num));
	$self->write_log("Generated RTK_XML: $fname");

	return 1;

}

sub get_xml_filenum
{
	my $self = shift;
	if (exists $self->{xml_filenum})
	{
		return $self->{xml_filenum};
	}
	else
	{
		$self->error ("Cant get xml_filenum from obj");
		return 0;
	}
}

sub add_info_to_bd
{
	my $self = shift;
	#Get region_hash
	my $hash_region;
	my $query = 'SELECT region, id FROM region';
	my $sth = $self->{dbh} -> prepare ($query) or do {$self->error("Error on prepare $query"); return 0}; 
	$sth->execute or do {$self->error("Error on prepare $query"); return 0};
	while (my ($key, $value) = $sth->fetchrow_array())
	{
		$hash_region->{$key} = $value;
	}

	my $dat = sprintf ("%04d-%02d-%02d %02d:%02d:%02d", $self->{dat}->{year}, $self->{dat}->{mon}, $self->{dat}->{mday}, $self->{dat}->{hour}, $self->{dat}->{min}, $self->{dat}->{sec});
	foreach my $region (%{$self->{client}})
	{
		foreach my $letter (@{$self->{client}->{$region}})
		{
			my $query = "INSERT INTO client_id  (id, shpi, fname, out_fname, region_id, dat) values ($letter->{userid}, $letter->{shpi}, '$letter->{source_fname}', '$letter->{out_fname}', $hash_region->{$region}, '$dat') 
			ON DUPLICATE KEY UPDATE shpi=$letter->{shpi}, fname='$letter->{source_fname}', out_fname='$letter->{out_fname}', region_id=$hash_region->{$region}, dat='$dat'"; 	
		$self->{dbh}->do($query) or do {$self->error("Cant execute $query"); return 0};
		}
	}

	return 1; 
}

sub app
{
	my $self = shift;
	my %opt = @_;
	my $num 				= $opt{-num}  					|| do {$self->error("You must define -num param for app function"); return 0};

	foreach my $region (keys %{$self->{pdf}})
	{
		my $dir = sprintf("%s/%s/%s", $self->{output_dir}, $region, $self->{formatted_date});
		my $app_fname = create_app (
							-dataref => $self->{pdf}->{$region}, 
							-dir => $dir, 
							-short_company_name => $self->{short_company_name}.'_'.$region,
							-company => $self->{company},
							-num => $num,
						) or do {$self->error("Cant create APP for region $region"); return 0};

		$self->{app}->{$region} = $app_fname;

	}
	return 1;
}

sub send_app
{
	my $self 		= shift;
	my %opt 		= @_;
	my $rcpt_hash 	= $opt{-rcpt} || do {$self->error("You must specify -rcpt param for function send_app"); return 0}; #Ссылка на хэш с перечислением регионов и получателей
	my $subj = sprintf ("Акт приема-передачи РТК от %02d.%02d.%04d", $self->{dat}->{mday}, $self->{dat}->{mon}, $self->{dat}->{year});
	my $body = $subj;
	foreach my $region (keys %{$self->{app}})
	{
		my $rcpt = $rcpt_hash->{$region};
		if (!$rcpt)
		{
			$self->error ("Not found rcpt for $region"); return 0;
		}
		my $mail 	= SendMail->new (
										-email 	=> $rcpt,
										-subj 	=> $subj,
										-body	=> $body,
										-attachment => [$self->{app}->{$region}],
									);
		if ($mail->send())
		{
			$self->write_log( "APP region $region to $rcpt send");
			return 1;
		}	
		else
		{
			$self->error ("Failed to send APP region $region to $rcpt");
		}
	}
}

sub send_f103
{
	my $self 		= shift;
	my %opt 		= @_;
	my $rcpt_hash 	= $opt{-rcpt} || do {$self->error("You must specify -rcpt param for function send_app"); return 0}; #Ссылка на хэш с перечислением регионов и получателей
	my $subj = sprintf ("Электронные списки РТК от %02d.%02d.%04d", $self->{dat}->{mday}, $self->{dat}->{mon}, $self->{dat}->{year});
	my $body = 'Просьба прогрузить электронные списки в ПО Партионная почта и сообщить результат';
	foreach my $region (keys %{$self->{ini}})
	{
		my $rcpt = $rcpt_hash->{$region};
		if (!$rcpt)
		{
			$self->error ("Not found rcpt for $region"); return 0;
		}
		my $mail 	= SendMail->new (
										-email 	=> $rcpt,
										-subj 	=> $subj,
										-body	=> $body,
										-attachment => $self->{ini}->{$region},
									);
		if ($mail->send())
		{
			$self->write_log ("F103 region $region to $rcpt send");
			return 1;
		}	
		else
		{
			$self->error("Failed to send F103 region $region to $rcpt");
			return 0;
		}
	}
}


sub get_shpi_count
{
	my $dbh 	= shift;
	my $region 	= shift;
	my $count 	= 0;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	#Вычисляем номер текущего месяца с 2000 года
	my $month = (($year - 100)*12) % 99 + $mon + 1;
	my $query = "select count(*) from shpi s inner join region r on r.id=s.region_id where mod(ceiling(s.shpi/1000000)-1, 100) = $month and r.region='$region'";
	my $sth = $dbh->prepare($query); $sth->execute;
	$count = $sth->fetchrow_array();
	return $count;
}

sub afp
{
	my $self 	= shift;
	my %opt 	= @_;
	my $cpus 	= $opt{-cpus} || 4;
	my ($pool, $finish) = create_pool ('pdf2afp', $cpus);
	foreach my $region (keys %{$self->{pdf}})
	{
		foreach my $pdf (@{$self->{pdf}->{$region}})
		{	
			my $afp = $pdf->{filename};
			$afp =~ s/\.pdf$/.afp/;
			$$pool->($pdf->{filename}, $afp, $FORMDEF, 'wine', sub {});
			push @{$self->{afp}->{$region}}, $afp;
		}	
	}
	undef $$pool; $$finish->recv;
 	return 1;

}


sub check_address
{
	my $address = shift;
	my $post = {};
	my $a = AddressInfo_new -> new ($address, -check_post_index => 1);
	$a -> get_info () or  die 'Cant get info about '.$a->source_address().' '.$a->error();
	if ($a->post_index())
#	if ($a->post_index() && $a->post_index_in_bd())
	{
		$post->{post_index}        	= $a->post_index();
		$post->{address} 			= $a->post_address();
		my $address_hash 			= $a->address_hash() or die "Cant get address_hash";
		foreach my $key (keys %$address_hash)
		{
			$post->{$key} = $address_hash->{$key};
		}
	}
	else
	{
		return 0;
	}

	return $post;
}

sub define_region
{#Функция оперделения региона из БД по таблице post_index_region
	my $self 		= shift;
	my $post_index 	= shift;
	my $default_region = 'MOSCOW';
	my $query = "select r.region from post_index_region p inner join region r on r.id=p.region_id where p.post_index=$post_index";
	my $sth = $self->{dbh}->prepare($query) or do {$self->error("Cant prepare $query ".$self->{dbh}->err); return 0};
	$sth->execute or do {$self->error("Cant execute $query ".$self->{dbh}->err); return 0};
	if (my $region = $sth->fetchrow_array())
	{
		return $region;
	}
	return $default_region;
}


sub error
{
	my $self = shift;
	if (@_)
	{
		$self->{error} = "@_";
		$self->notice(@_);
		$self->write_error (join '', @_);
	}
	else
	{	
		return $self->{error} || '';
	}
}

sub notice
{#Уведомление об ошибке
	my $self = shift;
	my $message = join (' ', @_);
	my $jabber_clients = [];
	my $query = "select jabber_id from notice where enable=1";
	my $sth = $self->{dbh}->prepare($query); $sth->execute;
	$jabber_clients = $sth->fetchall_arrayref([0]);

	foreach my $jabber_id (@$jabber_clients)
	{
		#$0 - имя программы
		Jabber::send(-message => join (": ", $0, date_with_time(), $message), -to => $jabber_id->[0]) or do {warn "Cant send message to $jabber_id"; return 0};
	}
	return 1;

}

sub upload
{#This method upload afp files and XML report to ftp server
	my $self = shift;
	my %opt = @_;
	my $ftp_dir = $opt{-dir} || do {$self->error("You must specify -dir param for method upload"); return 0};
	foreach my $region (keys %{$self->{afp}})
	{
		foreach my $afp (@{$self->{afp}->{$region}})
		{
			print "Upload $afp\n";
			my $dir = sprintf ("%s/%s", $ftp_dir, $region);
			scp_upload (-fname => $afp, -rdir => $dir, -subdir => $self->{formatted_date}) or do {$self->error("Cant upload $afp to $dir"); return 0};
		}
	}
#Upload xml
	scp_upload (-fname => $self->{xml_fname}, -rdir => join ('/', $ftp_dir, 'XML')) or do {$self->error("Cant upload xml for rtk $self->{xml_fname}"); return 0};

	return 1;
}

sub create_pool
{#Создает пул воркеров и возвращает на них ссылку
 # Функция воркера
	 my $sub 	= shift;
	 my $cpus 	= shift;
# Модуль воркера	
	 my $mod = 'Rostelecom';
# Создать пул воркеров
	 my $pool = AnyEvent::Fork
	 ->new
	->require ($mod)
	->AnyEvent::Fork::Pool::run(
			 "${mod}::$sub",         # Модуль::Функция - рабочая функция воркера
			  max  => $cpus,          # Количество воркеров в пуле
			  idle => 0,              # Количество воркеров при простое
			  load => 1,              # Размер очереди воркера
			  on_destroy => (my $finish = AE::cv),
						 );

	 return (\$pool, \$finish);
}

sub write_log
{
	my $self = shift;
	my $message = join (' ', @_);
	return 0 if not $self->{write_log};
	open my $FL, ">>:encoding(utf8)",  $self->{write_log} or do {$self->error ("Cant open logfile $self->{write_log}"); return 0};
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $tme = sprintf("%02d:%02d:%02d %02d/%02d/%04d", $hour, $min, $sec, $mday, $mon+1, $year+1900);
	print $FL "$tme $message\n";
	close $FL;
	return 1;

}

sub write_error
{
	my $self = shift;
	my $message = join (' ', @_);
	return 0 if not $self->{write_error};
	open my $FL, ">>:encoding(utf8)",  $self->{write_error} or return 0;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $tme = sprintf("%02d:%02d:%02d %02d/%02d/%04d", $hour, $min, $sec, $mday, $mon+1, $year+1900);
	print $FL "$tme $message\n";
	close $FL;
	return 0;
}
1;
