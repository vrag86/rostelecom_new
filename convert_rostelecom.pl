#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: convert_rostelecom.pl
#
#        USAGE: ./convert_rostelecom.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.05.2015 19:44:28
#     REVISION: ---
#===============================================================================

use strict;
#use warnings;
use utf8;
use FindBin qw ($Bin);
use lib '/home/vrag/perl/Modules';
use lib $Bin;
use File::Flock::Tiny;
use Config::INI::Writer;
use Config::INI::Reader;
use Net::SFTP::Foreign;
use Archive::Zip;
use Archive::Extract;
use Devel::Timer;
use File::Basename;
use Rostelecom;

our $OUTPUT_DIR		= "$Bin/output";
our $TEMPLATE 		= "$Bin/template/maket.pdf";
our $INPUT_DIR 		= "$Bin/input";
our $CONFIG_FILE 	= "$Bin/config.ini";
our $conf           = read_config();
our $LOG_FILE		= sprintf ("%s/%s", $Bin, $conf->{main}->{log_file}) || '';
our $ERROR_FILE 	= $conf->{main}->{error_file} || '';


our $ftp_server        = $conf->{main}->{ftp_server};
our $ntpserver         = $conf->{main}->{ntp_server};
our $ops = {
			'MOSCOW' => $conf->{MOSCOW}->{ops},
			'PITER' => $conf->{PITER}->{ops},
			'NOVOSIB' => $conf->{NOVOSIB}->{ops},
			};

our $t = Devel::Timer->new();


my $pid = File::Flock::Tiny -> write_pid($conf->{main}->{flock});
if (!$pid)
{
	print STDOUT "Rostelecom already running\n";
	exit 0;
}
else
{
	main();
	$pid->close;
}

sub main
{
	my $filenames;
	
	$t->mark("Download files from ftp");
	my $getting_files = check_remote_files (-server => $ftp_server, -ext => 'zip', -remote_dir => $conf->{main}->{input_dir}, -dest_dir => $INPUT_DIR);
	if (not @$getting_files)
	{
		print "New files not found. Dir: $conf->{main}->{input_dir}\n";
		return 0;
	}
	unlink glob "$INPUT_DIR/*.xml";
	print "Getting files @$getting_files\n";


	$t->mark("Unzip downloaded files");
	foreach (@$getting_files)
	{
		my $zip_filename = $_;
		my $dir          = $INPUT_DIR;
		my $fh_error = undef;
		{
			open(my $FH, '>', \$fh_error);
			local *STDERR = *$FH;
			my $zip = Archive::Zip -> new();
			my $res = $zip->read ($zip_filename);
			die  "Cant unzip $zip_filename Res: $res" if ($res != 0);
			my @members = $zip -> membersMatching( '.*\.xml' );
			foreach my $m (@members)
			{
				my $fname = "$INPUT_DIR/".basename ($m->{fileName});
				$zip -> extractMember($m->{fileName}, $fname);
				push @$filenames, $fname;
				print "Extracted $fname\n";
			}
			close $FH;
		}
			
		print "$zip_filename extracted to $dir\n";
	}
	

	rostelecom ($filenames);
	#Запишем изменнеия в config.ini
	Config::INI::Writer->write_file($conf, $CONFIG_FILE);	


   		
}


sub rostelecom
{
	my $filenames = shift;
	my $app_rcpt;
	my $ops;
	my $f103_rcpt;
	foreach my $region (grep {$_ ne 'main'} keys %$conf)
	{
		$app_rcpt->{$region} = $conf->{$region}->{app_rcpt};
		$ops->{$region} = $conf->{$region}->{ops};
		$f103_rcpt->{$region} = $conf->{$region}->{f103_rcpt};

	}
	my $rtk = Rostelecom->new(  -output_dir 			=> $OUTPUT_DIR,
								-template				=> $TEMPLATE,
								-company				=> $conf->{main}->{company},
								-inn 					=> $conf->{main}->{inn},
								-kpp 					=> $conf->{main}->{kpp},
								-short_company_name 	=> $conf->{main}->{short_company_name},
								-cpus				 	=> $conf->{main}->{cpus},
								-post_index_sndr	 	=> $conf->{main}->{post_index_sndr},
								-sender_region	 		=> $conf->{main}->{sender_region},
								-sender_city		 	=> $conf->{main}->{sender_city},
								-write_log				=> $LOG_FILE,
								-write_error			=> $ERROR_FILE,
							) or die "Cant create obj Rostelecom";

	$t->mark("Read data and validate address. Function: Rostelecom::read_data");
	foreach my $fname (sort @$filenames)
	{
		my @res = $rtk->read_data($fname);
		
		die "Cant read $fname".$rtk->error if not $res[0];
		printf "Fname: %s Reads ok: %d. Reads bad: %d\n", basename($fname), $res[0], $res[1];
	}

	$t->mark("Define shpi. Function: Rostelecom::define_shpi");
	$rtk->define_shpi() or die "Cant define shpi".$rtk->error;
	$t->mark("Create_pdf. Function: Rostelecom::create_pdf");
	$rtk->create_pdf(-spool_size => $conf->{main}->{spool_size}, -ops=> $ops) or die "Cant create pdf files".$rtk->error;
	
	$t->mark("Generate rtk_xml. Function: Rostelecom::generate_rtk_xml");
	$rtk->generate_rtk_xml($conf->{main}->{xml_filenum}) or die "Cant generate RTK xml ".$rtk->error;
	$conf->{main}->{xml_filenum} = $rtk->get_xml_filenum() or die "Cant get xml_filenum ".$rtk->error;
	
	$t->mark("Add_info_to_bd. Function: Rostelecom::add_info_to_bd");
	$rtk->add_info_to_bd() or die "Cant add info to bd".$rtk->error;
	$t->mark("Create app. Function: Rostelecom::app");
	$rtk->app(-num => ++$conf->{main}->{lists_num}) or die "Cant create app ".$rtk->error;
	$t->mark("Send f103 to email. Function: Rostelecom::send_f103");
	$rtk->send_f103(-rcpt => $f103_rcpt) or die "Cant send f103 ". $rtk->error;
	$t->mark("Convert pdf to afp. Function: Rostelecom::afp");
	$rtk->afp() or die "Cant convert pdf files to afp". $rtk->error;
	$t->mark("Upload to ftp. Function: Rostelecom::upload");
	$rtk->upload(-dir => $conf->{main}->{output_dir}) or die "Cant upload files to $conf->{main}->{output_dir} ". $rtk->error;
	$t->mark("Send app to email. Function: Rostelecom::send_app");
	$rtk->send_app(-rcpt => $app_rcpt) or die "Cant send app ".$rtk->error;
 

	$t->mark ('END');
	$t->report();
								
}


sub read_config
{
#read config file
	my $c = Config::INI::Reader->read_file($CONFIG_FILE);
	#Проверить, все поля инициализированы
	my @fields_main = qw /company ftp_server ntp_server inn input_dir flock short_company_name cpus spool_size lists_num output_dir kpp post_index_sndr sender_region sender_city/;
	my @region      = qw /MOSCOW PITER NOVOSIB/;
	my @fields_region = qw /ops app_rcpt f103_rcpt/;
	foreach my $f (@fields_main)
	{
		die "$CONFIG_FILE not contain $f" if (!exists ($c->{main}->{$f}));
	}

	foreach my $r (@region)
	{
		foreach my $f (@fields_region)
		{
			die "$CONFIG_FILE not contain $r $f" if (!exists ($c->{$r}->{$f}));
		}
	}
	return $c;
}

sub check_remote_files
{
	my %opt				= @_;
	my $server			= $opt{-server} 		|| do {warn "You must specify -server param for check_remote_files functions"; return 0};
	my $remote_dir  	= $opt{-remote_dir} 	|| do {warn "You must specify -remote_dir param for check_remote_files functions"; return 0};
	my $dest_dir    	= $opt{-dest_dir}		|| do {warn "You must specify -dest_dir param for check_remote_files functions"; return 0};
	my $ext   		 	= $opt{-ext} 			|| do {warn "You must specify -ext param for check_remote_files functions"; return 0};

	my $get_files = [];
#Хэш, содержащий имя файла и его размер
	my %filenames;          
	my $sftp = Net::SFTP::Foreign -> new (host => $server, user => 'vrag') or die "Cant connect to $server";
	$sftp->die_on_error("Unable to establish SFTP connection");
#	write_log ("Connected to $server. Dir: $remote_dir");
	
	my $exit_status = 0;
	do 
	{	
		$exit_status = 0;
		my $ls = $sftp -> ls ($remote_dir) or die "Cant ls $remote_dir" . $sftp -> error;
		foreach my $f (@$ls)
		{
			my $fname = $f->{filename};
			my $size  = $f->{a}->size;
			next if ($fname =~ /^\./ || !($fname =~ /\.$ext$/));

			if (!exists $filenames{ $fname })
			{
				$filenames{$fname} = $size;
				$exit_status = 1;
				print "Found new file $fname size $size\n";
			}	
			elsif ($filenames{$fname} != $size)
			{
				print "New size of $fname = $size\n";
				$filenames{$fname} = $size;
				$exit_status = 1;
			}
		}
		sleep 30 if $exit_status == 1;
	} while ($exit_status);

	foreach my $key (keys %filenames)
	{
		print "Found file $key size $filenames{$key}\n";
	}

	foreach my $fname (keys %filenames)
	{
		my $remote_fname = $remote_dir.'/'.$fname;
		$sftp -> get ($remote_fname, $dest_dir.'/'.$fname, cleanup => 1, overwrite => 1, ) or die  "Cant get $remote_fname";
		$sftp -> remove ($remote_fname) or write_log ("Cant remove remote file $remote_fname", 1);
		push @$get_files, "$dest_dir/$fname";
		print "File $remote_fname downloaded to $dest_dir/$fname\n";
	}
	
	return $get_files;


}

