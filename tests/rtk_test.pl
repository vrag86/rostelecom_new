#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: rtk_test.pl
#
#        USAGE: ./rtk_test.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 08.05.2015 14:15:04
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use FindBin qw ($Bin);
use lib "$Bin/../";
use lib '/home/vrag/perl/Modules';
use Test::More qw(no_plan);
#use Test::More tests	=> 21;
use File::Basename;
use XML::Simple;
use Dat;
use Devel::Timer;

our $INPUT_DIR = "$Bin/input";
our $OUTPUT_DIR = "$Bin/output";
our $TEMPLATE	= "$Bin/../template/maket.pdf";
our $spool_size = 500;
our $company = 'ОАО "Ростелеком"';
our $log_file = "$Bin/rtk_log.txt";
our $error_file = "$Bin/rtk_error.txt";
our $DB_NAME 	= 'rostelecom_test';

my $ops = {MOSCOW => 'Test OPS MOSCOW', PITER=> 'Test OPS Piter'};
my @bad_xml = glob "$Bin/bad/*.xml";

my $t = Devel::Timer->new();

$t->mark ('use Rostelecom');
use_ok ('Rostelecom');

Rostelecom::set_param (-db_name => $DB_NAME);

$t->mark ('Create Rostelecom obj');
my $rtk = Rostelecom->new (-output_dir => $OUTPUT_DIR, 
							-template => $TEMPLATE, 
							-company => $company, 
							-inn => 77777, 
							-short_company_name => 'RTK_test',
							-write_log		=> $log_file,
							-write_error	=> $error_file,
							-cpus => 16);
isa_ok ($rtk, 'Rostelecom');


$t->mark ('Fail_create Rostelecom obj');
my $rtk_fail = Rostelecom->new();
ok (!$rtk_fail, 'Test for fail rtk create obj');


#Test for read bad file
#$t->mark ('Read bad file');
#test_read_bad ($rtk);



foreach my $fname (glob "$INPUT_DIR/*.xml")
{
	$t->mark ("Read data $fname");
	my @res = $rtk->read_data($fname);
	
	ok ($res[0], "Test for read data $fname");
	print "Fname: ", basename($fname), " Reads ok $res[0]. Reads bad $res[1]\n";
}

$t->mark ('Define shpi');
ok ($rtk->define_shpi(), "Test for define shpi");

$t->mark ('create_pdf');
ok ($rtk->create_pdf(-spool_size => 500, -ops=> $ops), 'Test for create pdf files');
#Test fail1

$t->mark ('Fail create pdf');
ok (!$rtk->create_pdf(), 'Test for fail1 create_pdf');
like ($rtk->error, qr/-spool_size/, 'Test for fail_message1');
delete $rtk->{error};

$t->mark ('Generate RTK xml');
ok ($rtk->generate_rtk_xml('20150230;1'), 'Test for generate_rtk_xml');
die $rtk->error if $rtk->error;


$t->mark ('Add info to bd');
ok ($rtk->add_info_to_bd(), 'Test for add info_to_bd');
die $rtk->error if $rtk->error;

$t->mark ('Create app');
ok ($rtk->app(-num => 17 ), 'Test for create APP');
die $rtk->error if $rtk->error;
ok (-e $rtk->{app}->{MOSCOW}, 'Test for exists APP file');

$t->mark ('Send app');
ok ($rtk->send_app(-rcpt => {MOSCOW => 'vrag86@mail.ru'}), 'Test for send APP');
die $rtk->error if $rtk->error;

$t->mark ('Send f103');
ok ($rtk->send_f103(-rcpt => {MOSCOW => 'vrag86@mail.ru'}), 'Test for send f103');
die $rtk->error if $rtk->error;

$t->mark ('Convert to afp');
ok ($rtk->afp(), 'Test for convert pdf to afp');
my $i = 0;
foreach my $region (keys %{$rtk->{afp}})
{
	foreach my $afp (@{$rtk->{afp}->{$region}})
	{
		ok (-e $afp, "Test for exists afp file ".++$i);
	}
}
die $rtk->error if $rtk->error;



$t->mark ('Upload to ftp');
ok ($rtk->upload(-dir => '/ftp/ftp-shared/CGPSPB/Temp/rtk_test'), 'Test for upload files to ftp server');
die $rtk->error if $rtk->error;

$t->mark ('END');
#$t->report();


sub test_read_bad
{
	my $rtk = shift;
	my $i = 1;
	foreach my $bad (@bad_xml)
	{
		ok (!$rtk->read_data($bad), "Test for read bad file $i");
		like ($rtk->error, qr/correct_data error/, "Test for bad message $i");
		delete $rtk->{error};
		$i++;
	}

}
